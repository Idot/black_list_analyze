package lib

import (
	"errors"
	"log"
)

var (
	FORMAT_DATE     = "2006-01-02"
	FORMAT_DATETIME = "2006-01-02 15:04:05"
)

func CheckFatalErr(reason string, err error) {
	if err != nil {
		log.Fatalln(reason, "->", err)
	}
}

func PackgeErr(location string, err error) error {
	if err != nil {
		return errors.New(location + ": " + err.Error())
	}
	return nil
}
