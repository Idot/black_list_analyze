package syslog

import (
	"time"

	"black_list_analyze/lib"
	"black_list_analyze/lib/db/blacklist"
)

func LastStartTime() (string, error) {
	logs, err := blacklist.GetLatelyOKSystemLogs(1)
	if err != nil {
		return "", lib.PackgeErr("LastStartTime", err)
	}
	if logs == nil || len(logs) == 0 {
		return "", nil
	}
	return logs[0].StartTime, nil
}

func Start() (log blacklist.SystemLog, err error) {
	err = blacklist.InsertSystemLog(time.Now().Format(lib.FORMAT_DATETIME))
	if err != nil {
		err = lib.PackgeErr("Start", err)
		return
	}
	logs, err := blacklist.GetLatelySystemLogs(1)
	if err != nil {
		err = lib.PackgeErr("Start", err)
		return
	}
	return logs[0], nil
}

func End(id int, descr string) error {
	err := lib.PackgeErr("End", blacklist.UpdateSystemLog(id, time.Now().Format(lib.FORMAT_DATETIME), descr))
	return err
}
