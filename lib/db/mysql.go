package db

import (
	"database/sql"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var (
	MODE_DEBUG bool
)

func GetMysqlConn(dsn string) (db *sql.DB, err error) {
	db, err = sql.Open("mysql", dsn)
	if err != nil {
		return
	}

	err = db.Ping()
	return
}

func GetMysqlConnX(dsn string) (db *sqlx.DB, err error) {
	db, err = sqlx.Open("mysql", dsn)
	if err != nil {
		return
	}

	err = db.Ping()
	return
}

func Debug(v ...interface{}) {
	if MODE_DEBUG {
		log.Println(v...)
	}
}
