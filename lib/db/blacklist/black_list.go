package blacklist

import (
	"black_list_analyze/conf"
	"black_list_analyze/lib"
	"black_list_analyze/lib/db"

	"github.com/jmoiron/sqlx"
)

var (
	conn *sqlx.DB
)

func init() {
	var err error
	var dsn string

	dsn, err = conf.Cfg.String("database.dsn.blacklist")
	lib.CheckFatalErr("[lib/db/blacklist] init black list dsn", err)

	conn, err = db.GetMysqlConnX(dsn)
	lib.CheckFatalErr("[lib/db/blacklist] init black list db connect", err)
}
