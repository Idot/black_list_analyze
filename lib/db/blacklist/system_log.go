package blacklist

import (
	"database/sql"

	"black_list_analyze/lib"
)

type SystemLog struct {
	Id        int
	StartTime string         `db:"start_time"`
	EndTime   sql.NullString `db:"end_time"`
	Descr     sql.NullString
}

func GetLatelySystemLogs(size int) (logs []SystemLog, err error) {
	query := "select * from log_system order by start_time desc limit 0,?"
	err = lib.PackgeErr("GetLatelySystemLogs", conn.Select(&logs, query, size))
	return
}

func GetLatelyOKSystemLogs(size int) (logs []SystemLog, err error) {
	query := "select * from log_system where descr='ok' order by start_time desc limit 0,?"
	err = lib.PackgeErr("GetLatelySystemLogs", conn.Select(&logs, query, size))
	return
}

func InsertSystemLog(startTime string) error {
	query := "insert into log_system(start_time) values(?)"
	_, err := conn.Exec(query, startTime)
	return err
}

func UpdateSystemLog(id int, endTime, descr string) error {
	query := "update log_system set end_time=?, descr=? where id=?"
	_, err := conn.Exec(query, endTime, descr, id)
	return err
}
