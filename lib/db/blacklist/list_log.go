package blacklist

import (
	"database/sql"
	"fmt"

	"black_list_analyze/lib"
)

type ListLog struct {
	Uid        string         `db:"user_id"`
	Id         string         `db:"id"`
	TypeId     int            `db:"type_id"`
	SourceId   int            `db:"source_id"`
	AppId      string         `db:"app_id"`
	ExpireDays sql.NullInt64  `db:"expire_days"`
	UpdateDate string         `db:"update_date"`
	Reason     sql.NullString `db:"reason"`
}

func GetBlackListLogs(beginDate, endDate string) (logs []ListLog, err error) {
	logs, err = getListLogs(beginDate, endDate, "log_list_black")
	err = lib.PackgeErr("GetBlackListLogs", err)
	return
}

func GetGrayListLogs(beginDate, endDate string) (logs []ListLog, err error) {
	logs, err = getListLogs(beginDate, endDate, "log_list_gray")
	err = lib.PackgeErr("GetGrayListLogs", err)
	return
}

func GetLimitBlackListLogs(beginDate, endDate string, index, size int) (logs []ListLog, err error) {
	logs, err = getLimitListLogs(beginDate, endDate, "log_list_black", index, size)
	err = lib.PackgeErr("GetLimitBlackListLog", err)
	return
}

func GetLimitGrayListLogs(beginDate, endDate string, index, size int) (logs []ListLog, err error) {
	logs, err = getLimitListLogs(beginDate, endDate, "log_list_gray", index, size)
	err = lib.PackgeErr("GetLimitGrayListLog", err)
	return
}

func getListLogs(beginDate, endDate, table string) (logs []ListLog, err error) {
	query := fmt.Sprintf("select * from %s where update_date >= ? and update_date < ?", table)
	err = conn.Select(&logs, query, beginDate, endDate)
	return
}

func getLimitListLogs(beginDate, endDate, table string, index, size int) (logs []ListLog, err error) {
	query := fmt.Sprintf("select * from %s where update_date >= ? and update_date < ? limit ?,?", table)
	err = conn.Select(&logs, query, beginDate, endDate, index, size)
	return
}
