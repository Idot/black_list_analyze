package blacklist

import (
	"black_list_analyze/lib"
)

type Id struct {
	Id         int
	Descr      string
	UpdateDate string `db:"update_date"`
}

func GetAllTypeIds() (ids []Id, err error) {
	query := "select * from identify_type"
	err = lib.PackgeErr("GetAllTypeIds", conn.Select(&ids, query))
	return
}

func GetAllSourceIds() (ids []Id, err error) {
	err = lib.PackgeErr("GetAllSourceIds", conn.Select(&ids, "select id,descr,update_date from identify_source"))
	return
}
