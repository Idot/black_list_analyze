package main

import (
	"log"

	"black_list_analyze/lib"
	"black_list_analyze/lib/analyzer"
)

func main() {
	lib.CheckFatalErr("Analyze", analyzer.Analyze())
	log.Println("done")
}
