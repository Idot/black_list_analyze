package conf

import (
	"flag"
	"log"

	"github.com/olebedev/config"
)

var (
	Cfg *config.Config

	path_config string
)

func init() {
	flag.StringVar(&path_config, "c", "", "-c config")
	flag.Parse()
	if path_config == "" {
		path_config = "config.json"
	}
	var err error
	Cfg, err = config.ParseJsonFile(path_config)
	if err != nil {
		log.Fatalln("init config file->", err)
	}
}
